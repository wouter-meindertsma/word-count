package nl.meindertsma.ordina.wordcount.domein;

public interface WordFrequencyInterface {
    String getWord();
    int getFrequency();
}

package nl.meindertsma.ordina.wordcount.domein;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WordFrequency implements WordFrequencyInterface {

    private String word;

    private int frequency;

    @Override
    public String getWord() {
        return word;
    }

    @Override
    public int getFrequency() {
        return frequency;
    }
}

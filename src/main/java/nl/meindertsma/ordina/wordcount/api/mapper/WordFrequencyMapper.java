package nl.meindertsma.ordina.wordcount.api.mapper;

import nl.meindertsma.ordina.wordcount.api.dto.WordFrequencyDto;
import nl.meindertsma.ordina.wordcount.domein.WordFrequency;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface WordFrequencyMapper {

    @Named("toDto")
    WordFrequencyDto map (WordFrequency wordFrequency);

    @IterableMapping(qualifiedByName="toDto")
    List<WordFrequencyDto> map(List<WordFrequency> calculateMostFrequentNWords);
}

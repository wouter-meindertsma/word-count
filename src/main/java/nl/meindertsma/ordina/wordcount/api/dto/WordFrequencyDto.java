package nl.meindertsma.ordina.wordcount.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WordFrequencyDto {
    private String word;
    private int frequency;
}

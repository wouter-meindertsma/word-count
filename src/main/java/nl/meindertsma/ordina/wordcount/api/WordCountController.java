package nl.meindertsma.ordina.wordcount.api;

import lombok.AllArgsConstructor;
import nl.meindertsma.ordina.wordcount.api.dto.WordFrequencyDto;
import nl.meindertsma.ordina.wordcount.api.mapper.WordFrequencyMapper;
import nl.meindertsma.ordina.wordcount.service.WordFrequencyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@Validated
@AllArgsConstructor
@RequestMapping("/word-count")
public class WordCountController {

    private static final String MESSAGE_BODY = "{\"message\": \"";

    private final WordFrequencyService wordFrequencyService;

    private final WordFrequencyMapper mapper;

    @GetMapping(path = "/highest-frequency", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WordFrequencyDto> calculateHighestFrequency (@Valid @RequestParam(value = "text", required = true) @Max(300) final String text) throws IllegalAccessException {
        if (text.isEmpty()) {
            throw new IllegalArgumentException("Text may not be empty.");
        }

        final var highestFrequency = wordFrequencyService.calculateHighestFrequency(text);
        return new ResponseEntity<>(WordFrequencyDto.builder().frequency(highestFrequency).build(), HttpStatus.OK);
    }

    @GetMapping(path = "/frequency-for-word", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<WordFrequencyDto> calculateFrequencyForWord (@Valid @RequestParam(value = "text", required = true) @Max(300) @NotBlank final String text,
                                                                       @Valid @RequestParam(value = "word", required = true) @Pattern(regexp = "^([a-zA-Z])", message = "Geen geldige invoer woord") @Max(25) @NotBlank final String word) throws IllegalAccessException {
        if (text.isEmpty() || (word.isEmpty())) {
            throw new IllegalArgumentException("Text may not be empty. Word ("+ word + ") needs to be alphabetical only");
        }

        final var frequencyForWord = wordFrequencyService.calculateFrequencyForWord(text, word);
        return new ResponseEntity<>(WordFrequencyDto.builder().frequency(frequencyForWord).build(), HttpStatus.OK);

    }

    @GetMapping(path = "/highest-frequency-words", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<WordFrequencyDto>> calculateMostFrequentNWords (@Valid @RequestParam(value = "text", required = true) @Max(300) final String text, @RequestParam("quantityOfWords") @Max(10) final int quantityOfWords) throws IllegalAccessException {
        if (text.isEmpty() || (quantityOfWords <= 0 || quantityOfWords > 10)) {
            throw new IllegalArgumentException("Text may not be empty. Quantity of words needs to be higher than 0");
        }

        final var mostFrequentWords = mapper.map(wordFrequencyService.calculateMostFrequentNWords(text, quantityOfWords));
        return new ResponseEntity<>(mostFrequentWords, HttpStatus.OK);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(final IllegalArgumentException e) {
        final String body = MESSAGE_BODY + "Word count has failed: "+ e +"\"}";

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
    }

}

package nl.meindertsma.ordina.wordcount.service;

import lombok.AllArgsConstructor;
import nl.meindertsma.ordina.wordcount.domein.WordFrequency;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class WordFrequencyService implements WordFrequencyAnalyzer{
    @Override
    public int calculateHighestFrequency(String text) {
        final Map<String, Long> wordFrequencyMap = getWordsAndFrequencies(text);

        return wordFrequencyMap.values().stream().mapToInt(Math::toIntExact).max().orElse(0);
    }

    @Override
    public int calculateFrequencyForWord(String text, String word) {
        final Map<String, Long> wordFrequencyMap = getWordsAndFrequencies(text);

        return Math.toIntExact(wordFrequencyMap.getOrDefault(word.toLowerCase(), Long.valueOf(0)));
    }

    @Override
    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
        final Map<String, Long> wordFrequencyMap = getWordsAndFrequencies(text);

        List<Map.Entry<String, Long>> lijst = wordFrequencyMap.entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue(Comparator.reverseOrder())
                        .thenComparing(Map.Entry.comparingByKey()))
                .limit(n).toList();

        return lijst.stream()
                .map(e -> {
                    return WordFrequency.builder()
                            .word(e.getKey())
                            .frequency(e.getValue().intValue())
                            .build();
                })
                .toList();
    }

    private Map<String, Long> getWordsAndFrequencies(String text) {
        String textWithoutExtraCharacters = text.replaceAll("[!?,.]", "");
        String[] wordsFromString = textWithoutExtraCharacters.split("\\s+");
        return Arrays.stream(wordsFromString)
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}

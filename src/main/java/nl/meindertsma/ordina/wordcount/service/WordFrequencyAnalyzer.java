package nl.meindertsma.ordina.wordcount.service;

import nl.meindertsma.ordina.wordcount.domein.WordFrequency;

import java.util.List;

public interface WordFrequencyAnalyzer {

    int calculateHighestFrequency(String text);
    int calculateFrequencyForWord (String text, String word);
    List<WordFrequency> calculateMostFrequentNWords (String text, int n);
}

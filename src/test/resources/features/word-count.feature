#language:nl

Functionaliteit: Word Count

  Scenario: 1. Happy Flow

    Als ik de highest-frequency service aanroep met de volgende attributen
      | text | The sun shines over the lake  |
    Dan verwacht ik json response met exact onderstaande inhoud
    """
    {
      "frequency" : 2
    }
    """
    Als ik de frequency-for-word service aanroep met de volgende attributen
      | text | The sun shines over the lake  |
      | word | THE                           |
    Dan verwacht ik json response met exact onderstaande inhoud
    """
    {
      "frequency" : 2
    }
    """
    Als ik de frequency-for-word service aanroep met de volgende attributen
      | text | The sun shines over the lake  |
      | word | sUn                           |
    Dan verwacht ik json response met exact onderstaande inhoud
    """
    {
      "frequency" : 1
    }
    """
    Als ik de highest-frequency-words service aanroep met de volgende attributen
      | text            | The sun shines over the lake  |
      | quantityOfWords | 3                             |
    Dan verwacht ik json list response met exact onderstaande inhoud
    """
    [ {
      "word" : "the",
      "frequency" : "2"
    }, {
      "word" : "lake",
      "frequency" : "1"
    }, {
      "word" : "over",
      "frequency" : "1"
    } ]
    """


package nl.meindertsma.ordina.wordcount.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/features"},
        glue = "nl.meindertsma.ordina.wordcount.cucumber.steps")
@ActiveProfiles("test")
public class CucumberApplicationTests {
}

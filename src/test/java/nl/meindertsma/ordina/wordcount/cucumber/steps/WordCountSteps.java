package nl.meindertsma.ordina.wordcount.cucumber.steps;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.nl.Als;
import io.cucumber.java.nl.Dan;
import io.cucumber.java.nl.Stel;
import nl.meindertsma.ordina.wordcount.WordcountApplication;
import nl.meindertsma.ordina.wordcount.cucumber.util.TestHelper;
import org.json.JSONArray;
import org.junit.Assert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import ro.skyah.comparator.CompareMode;
import ro.skyah.comparator.JSONCompare;

import java.util.HashMap;
import java.util.Map;

import static ro.skyah.comparator.CompareMode.JSON_ARRAY_NON_EXTENSIBLE;
import static ro.skyah.comparator.CompareMode.JSON_OBJECT_NON_EXTENSIBLE;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {WordcountApplication.class})
public class WordCountSteps extends TestHelper {

    @Before
    @Stel("^de service is gestart$")
    public void deServiceIsGestart() {
        restTemplate.getRestTemplate().setErrorHandler(new DefaultResponseErrorHandler());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    @Als("^ik de (.*) service aanroep met de volgende attributen$")
    public void ikDeServiceAanroepMetAttributen(String service, DataTable inhoud) {
        Map<String, String> params = new HashMap<>();

        StringBuilder queryParams = new StringBuilder();
        boolean firstParam = true;

        if (inhoud != null) {
            Map<String, String> teVersturenAttributen = inhoud.asMap(String.class, String.class);
            for (Map.Entry<String, String> teVersturenAttribuut : teVersturenAttributen.entrySet()) {
                String value = teVersturenAttribuut.getValue();
                params.put(teVersturenAttribuut.getKey(), value);
                if (firstParam) {
                    queryParams = new StringBuilder("?" + teVersturenAttribuut.getKey() + "=" + value);
                } else {
                    queryParams.append("&").append(teVersturenAttribuut.getKey()).append("=").append(value);
                }
                firstParam = false;
            }
        }

        try {
            responseEntity = null;
            final String url = "/word-count/" + service + queryParams;

            responseEntity = restTemplate.getForEntity(url, String.class, params);
            jsonArray = new JSONArray();
            jsonObject = null;
            if (responseEntity.getBody() != null) {
                responsebodyUitlezen();
            }

            statusCode = responseEntity.getStatusCode().value();
        } catch (HttpServerErrorException | HttpClientErrorException e) {
            statusCode = e.getStatusCode().value();
        }
    }

    @Dan("^verwacht ik json response met (exact|deels) onderstaande inhoud$")
    public void verwachtIkJsonResponse(String exact, String inhoud) {
        verwachtIkEenResponseMetStatuscode(200);

        if (exact.equals("exact")) {
            JSONCompare.assertEquals(inhoud, jsonObject.toString(), CompareMode.JSON_ARRAY_NON_EXTENSIBLE, CompareMode.JSON_OBJECT_NON_EXTENSIBLE);
        } else {
            JSONCompare.assertEquals(inhoud, jsonObject.toString());
        }
    }

    @Dan("^verwacht ik json list response met (exact|deels) onderstaande inhoud$")
    public void verwachtIkJsonListResponse(String exact, String inhoud) {
        verwachtIkEenResponseMetStatuscode(200);

        if (exact.equals("exact")) {
            JSONCompare.assertEquals(inhoud, jsonArray.toString(), JSON_ARRAY_NON_EXTENSIBLE, JSON_OBJECT_NON_EXTENSIBLE);
        } else {
            JSONCompare.assertEquals(inhoud, jsonArray.toString(), JSON_ARRAY_NON_EXTENSIBLE);
        }
    }

    @Dan("^verwacht ik een response met statuscode (.*)$")
    public void verwachtIkEenResponseMetStatuscode(int statuscode) {
        Assert.assertEquals(statuscode, statusCode);
    }
}

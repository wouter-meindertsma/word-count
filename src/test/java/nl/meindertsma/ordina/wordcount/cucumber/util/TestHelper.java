package nl.meindertsma.ordina.wordcount.cucumber.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

public abstract class TestHelper {

    @Autowired
    protected TestRestTemplate restTemplate;

    protected final ObjectMapper objectMapper = new ObjectMapper();

    protected String xmlObject;
    protected String xlsFileName;
    protected JSONObject jsonObject;
    protected JSONArray jsonArray;
    protected ResponseEntity<String> responseEntity;
    protected int statusCode;


    protected void responsebodyXmlUitlezen() {
        try {
            xmlObject = responseEntity.getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void responsebodyUitlezen() {
        try {
            jsonArray = new JSONArray(Objects.requireNonNull(responseEntity.getBody()));
            jsonObject = (JSONObject) jsonArray.get(0);
        } catch (JSONException js) {
            try {
                jsonObject = new JSONObject(Objects.requireNonNull(responseEntity.getBody()));
            } catch (Exception ignored) { /* Als er geen uuid in staat dan is er niks aan de hand */ }
            try {
                jsonObject = new JSONObject(Objects.requireNonNull(responseEntity.getBody()));
            } catch (Exception ignored) { /* Als er geen uuid in staat dan is er niks aan de hand */ }
        } catch (ClassCastException ignored) {
            jsonObject = null;
        }
    }
}

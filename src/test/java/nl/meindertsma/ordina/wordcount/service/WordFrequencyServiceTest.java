package nl.meindertsma.ordina.wordcount.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class WordFrequencyServiceTest {

    private String textMock = "Lorem ipsum dolor sit amet. Aut consequatur commodi et maiores omnis non sint maiores et quia laboriosam in quia corrupti et atque voluptas. Sit error porro et perferendis nobis ut quibusdam dolor non sapiente dolorem et quia maiores nam voluptatem debitis." +
            "Quo minus officia ut molestiae voluptatem aut deleniti laudantium a tempora saepe est quia dolor aut distinctio excepturi et sunt incidunt. In natus doloribus aut velit temporibus id perspiciatis illum. Rem molestiae corporis ut velit animi ut quaerat voluptates nam sint sint eos nesciunt quae? Sed nostrum necessitatibus et repellat numquam et magnam beatae non corporis hic natus atque?" +
            "Ea illo repellendus qui incidunt autem et dolorem quod. Vel fugiat inventore et itaque provident ad consectetur fuga qui beatae obcaecati qui eveniet deleniti.";

    @InjectMocks
    private WordFrequencyService wordFrequencyService;

    @Test
    void calculateHighestFrequency() {
        var response = wordFrequencyService.calculateHighestFrequency(textMock);
        assertEquals(10, response);
    }

    @Test
    void calculateFrequencyForWord() {
        final String word1 = "ET";
        final String word2 = "uT";
        final String word3 = "sIt";
        final String notExistingWord = "notExistingWord";

        var response1 = wordFrequencyService.calculateFrequencyForWord(textMock, word1);
        var response2 = wordFrequencyService.calculateFrequencyForWord(textMock, word2);
        var response3 = wordFrequencyService.calculateFrequencyForWord(textMock, word3);
        var response4 = wordFrequencyService.calculateFrequencyForWord(textMock, notExistingWord);

        assertEquals(10, response1);
        assertEquals(4, response2);
        assertEquals(2, response3);
        assertEquals(0, response4);
    }

    @Test
    void calculateMostFrequentNWords() {
        var response = wordFrequencyService.calculateMostFrequentNWords(textMock, 5);
        assertEquals("et", response.get(0).getWord());
        assertEquals("aut", response.get(1).getWord());
        assertEquals("quia", response.get(2).getWord());
        assertEquals("ut", response.get(3).getWord());
        assertEquals("dolor", response.get(4).getWord());
    }
}

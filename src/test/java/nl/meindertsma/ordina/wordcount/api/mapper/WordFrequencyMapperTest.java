package nl.meindertsma.ordina.wordcount.api.mapper;

import nl.meindertsma.ordina.wordcount.domein.WordFrequency;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WordFrequencyMapperTest {

    private WordFrequencyMapper mapper;

    @BeforeEach
    void setUp() {
        this.mapper = new WordFrequencyMapperImpl();
    }

    @Test
    void map() {
        var wordfrequency = WordFrequency.builder()
                .frequency(1)
                .word("word")
                .build();
        var response = mapper.map(wordfrequency);
        assertEquals(1, response.getFrequency());
        assertEquals("word", response.getWord());
    }

    @Test
    void testMap() {
        var wordfrequencyList = List.of(WordFrequency.builder()
                .frequency(1)
                .word("word1")
                .build(), WordFrequency.builder()
                .frequency(2)
                .word("word2")
                .build());
        var response = mapper.map(wordfrequencyList);
        assertEquals(1, response.get(0).getFrequency());
        assertEquals("word1", response.get(0).getWord());
        assertEquals(2, response.get(1).getFrequency());
        assertEquals("word2", response.get(1).getWord());
    }
}

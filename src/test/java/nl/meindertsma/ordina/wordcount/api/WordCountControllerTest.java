package nl.meindertsma.ordina.wordcount.api;

import nl.meindertsma.ordina.wordcount.api.dto.WordFrequencyDto;
import nl.meindertsma.ordina.wordcount.api.mapper.WordFrequencyMapper;
import nl.meindertsma.ordina.wordcount.api.mapper.WordFrequencyMapperImpl;
import nl.meindertsma.ordina.wordcount.domein.WordFrequency;
import nl.meindertsma.ordina.wordcount.service.WordFrequencyService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WordCountControllerTest {

    @InjectMocks
    private WordCountController wordCountController;

    @Mock
    private WordFrequencyService wordFrequencyService;

    @Mock
    private final WordFrequencyMapper mapper = new WordFrequencyMapperImpl();

    @Test
    void calculateHighestFrequency() throws IllegalAccessException {
        when(wordFrequencyService.calculateHighestFrequency("text")).thenReturn(1);

        var response = wordCountController.calculateHighestFrequency("text");

        assertEquals(HttpStatus.OK , response.getStatusCode());
        assertEquals(1, Objects.requireNonNull(response.getBody()).getFrequency());
    }

    @Test
    void calculateFrequencyForWord() throws IllegalAccessException {
        when(wordFrequencyService.calculateFrequencyForWord("text in text", "tEXt")).thenReturn(2);

        var response = wordCountController.calculateFrequencyForWord("text in text", "tEXt");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(2, Objects.requireNonNull(response.getBody()).getFrequency());
    }

    @Test
    void calculateHighestFrequency_noText() throws IllegalAccessException {
        assertThrows(IllegalArgumentException.class, () -> wordCountController.calculateHighestFrequency(""));
    }

    @Test
    void calculateFrequencyForWord_wordIsEmpty() throws IllegalAccessException {
        assertThrows(IllegalArgumentException.class, () -> wordCountController.calculateFrequencyForWord("text in text", ""));
    }

    @Test
    void calculateFrequencyForWord_textIsEmpty() throws IllegalAccessException {
        assertThrows(IllegalArgumentException.class, () -> wordCountController.calculateFrequencyForWord("", "word"));
    }

    @Test
    void calculateMostFrequentNWords_nIs0() throws IllegalAccessException {
        assertThrows(IllegalArgumentException.class, () -> wordCountController.calculateMostFrequentNWords("text in text", 0));
    }

    @Test
    void calculateMostFrequentNWords_nIsAMinValue() throws IllegalAccessException {
        assertThrows(IllegalArgumentException.class, () -> wordCountController.calculateMostFrequentNWords("text in text", -1));
    }

    @Test
    void calculateMostFrequentNWords_nIsAMoreThan11() throws IllegalAccessException {
        assertThrows(IllegalArgumentException.class, () -> wordCountController.calculateMostFrequentNWords("text in text", 11));
    }

    @Test
    void calculateMostFrequentNWords() throws IllegalAccessException {
        when(wordFrequencyService.calculateMostFrequentNWords("text in text", 2)).thenReturn(List.of(
                WordFrequency.builder()
                        .word("text")
                        .frequency(2)
                        .build(),
                WordFrequency.builder()
                        .word("in")
                        .frequency(1)
                        .build()));

        when(mapper.map(anyList())).thenReturn(List.of(
                WordFrequencyDto.builder()
                        .word("text")
                        .frequency(2)
                        .build(),
                WordFrequencyDto.builder()
                        .word("in")
                        .frequency(1)
                        .build()));

        var response = wordCountController.calculateMostFrequentNWords("text in text", 2);

        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertThat(response.getBody().size()).isEqualTo(2);
        assertEquals(2, response.getBody().get(0).getFrequency());
        assertEquals("text", response.getBody().get(0).getWord());
    }
}
